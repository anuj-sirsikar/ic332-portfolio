Anuj Sirsikar  m255838  

## Learning Goals:  

**I can explain the role that the network core plays vs the network edge.**  

The network core is the combination of packet switches and links that connects the systems on the network edge. The network edge are all the systems that comprise the Internet's end systems. For example, the network core contains all of the levels of ISPs while the network edge contains the clients, cell towers, and servers (things that use the internet). The network core delivers the internet to the network edge, which is comprised of the things that use the internet.  

**I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.**  

Access Networks - "the network that physically connects an end system to the first router on a path from the end system to any other distant end system" (textbook)  

DSL: type of broadband residential access that makes use of the local telephone company's (telco) existing telephone infrastructure. Therefore, a house will get DSL internet access from the same company that provides its wired local phone access so in that case the telco is also its ISP. The home has a DSL modem which converts digital data into high frequency tones for transmition over telephone wires to the telco's local central office. Telephone call and internet connection can share the DSL link because the signals are encoded at different frequencies. It is asymmetric because the downstream and upstream rates are different. DSL is made for when the home and telco local central office are 5-10 miles apart.  

Cable: other type of broadband residential access, however, it makes use of the cable television's existing cable television infrastructure. So, a house will get their internet from the same company that they get their cable tv from. Employs both fiber and coaxial cables so it is called a hybrid fiber coax (HFC). Needs Cable modems that work similarily to DSL modems. Cable internet is a shared broadcast medium which means "every packet sent by the head end travels downstream on every link to every home and every packet sent by a home travels on the upstream channel to the head end" (textbook). That means if lots of people are downloading a video at the same time then the actual rate of downloading will be much lower then the standard cable downstream rate.   

Fiber: an optical fiber path from the company's central office (CO) to the home. Much faster internet rates. Each home gets its own fiber path however there is one path coming from the CO and then it splits off to individual homes.   

Wireless: high speed residential access without costly and failure-prone cabling. Data is sent wirelessly from the provider's base station to a modem in the house which is connected to a WIFI wireless router.  

**I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.**  

Went over LCDR Down's video on the end-to-end packet delay problem.   

Queuing delay - waiting to be transmitted onto the link. Heavier traffic means longer delay.   

Transmission delay - amount of time required to transmit all of the packet's bits into the link. Packet can be transmitted only after all packets have arrived.  

Propagation delay - time required to propogate from the beginning of the link to the router. Propogation speed is the speed of light. Delay is distance between two routers divided by the propogation speed.   

How to improve packet delay:  
* Have less applications running at the same time  
* Increase bandwidth  
* Reduce spaces throught the whole connection (i.e. from the CO to the home and from the devices    to the routers and modems)  

**I can describe the differences between packet-switched networks and circuit-switched networks.**  

They are both ways to move data through a network of links and switches.  

Packet-switched networks involve sending messages between end states by breaking down longer messages into smaller chunks called packets. The packets move from source to destination via communication links and packets switches (routers and link-layer switches). Packet switches use store-and-forward transmission which means that the packet switch has to get the whole message before it can start transmitting the first bit onto the outbound link. This can sometimes reult in output buffers and queuing delays and because buffer space is finite, packet loss is also a possibility.  

Circuit-switched networks reserve all the resources needed along a path for the duration of the communication between end systems. In packet-switched networks that is not the case, hence the queuing. The textbook gives a good analogy: packet-switched networks are like a resturant that neither requires nor accepts reservations while circuit-switched networks require reservations. The connection between the sender and receiver (that must be made before hand) is called a circuit. A circuit established also means that a constant tranmission rate has been reserved for the duration of the connection. In a circuit-switched network, hosts are connected to switches so when a host wants to talk to another host, a end-to-end connection between the two hosts is established.  

Some folks say that packet-switching is not ideal for real-time services like calling because of its unpredictable delays. But other people like it because it offers better sharing of transmission capacity and is simpler, more efficient, and less costly than circuit-switching. Some people also argue that circuit switching is wastefull because the dedicated circuits are idle during silent periods.  

**I can describe how to create multiple channels in a single medium using FDM and TDM.**
        
For FDM, the frequency spectrum of a link is divided among the connections across the link and the link puts aside a frequency band to each connection for the duration of the connection. FDM is used by FM radio stations where each station gets a specific frequency band and each band has the same size bandwidth.   

For TDM, time is divided into frames of a fized duration and each frame has a fixed number of time slots. When a connection is established by the network across a link, one time slot in every frame is dedicated to that specific connection.  

FDM -> "each circuit continuously gets a fraction of the bandwidth" - textbook  
TDM -> "each circuit gets all the bandwidth periodically during brief intervals of time (slots)" -  textbook  
        
**I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.**  

There are Tier-1 companies that provide interent to Regional ISPs that provide internet to Local ISPs. Regional ISPs don't always get internet from just one Tier-1 ISP, they get interent from multiple which is called multi-homing. This is so that they are not dependant on only one Tier-1 ISP. Local ISPs do the same thing with Regional ISPs. In today's world, the Local ISPs are dissappearing and people are starting to get internet straight from the Tier-1 ISPs and/or Regional ISPs.ISPs on the same level interact through a process called peering where ISPs on the same level share internet and work together to meet the needs of the client. Instead of peering between two ISPs on the same level, multiple ISPs on the same level can all connect to an IXP to share data and information and internet.    

**I   can explain how encapsulation is used to implement the layered model of the Internet.**  

As a message moves from the host it starts at the host's application layer. That application layer message is sent to the transport layer where it gets some additional information which will be used by the receiver's transport layer. That comprises the transport layer segment which is sent to the network layer. The network layer adds information regarding the source and destination addresses which creates a network layer datagram. Then it moves to the link layer which adds information making a link layer frame. So as the message moves through these layers, it adds another level of information and then when it gets to the receiver, the message is de-encapsulated as it moves back down the layers and then the receiver's application layer can access the message.  


## Textbook Problems:  

**R1 - What is the difference between a host and an end system? List several different types of end systems? Is a Web server an end system? (1.1)**
        
A host is an end system. These include desktops, servers, TVs, watches, and anything that uses/connects to the internet. Yes, a web server is an end system because it uses the internet. 

**R4 - List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access. (1.2)**                            

5G and 4G wireless networks - wide-area wireless access 
DSL - home access
Cable - home access
Wireless LAN connected via Ethernet - enterprise access

**R11 - Suppose there is exactly one packet switch between a sending host and a**
**receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)(1.3)**

L/R1 + L/R2

**R12 - What advantage does a circuit-switched network have over a packet-switched network? What advanatages does TDM have over FDM in a circuit-switched network? (1.3)**

Curcuit-switched networks are better for real-time services like calls because they reserve all the needed resources before hand so there is no delay. Along with resources for the the duration of the connection, a constant tranmission rate is also reserved. 

TDM can be better because each circuit gets all the bandwidth for the time it is being used. 

**R13 - Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3.)**
a. When circuit switching is used, how many users can be supported?

2

b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?

Because the most number of users tranmitting at 1 Mbps that the link can support is 2 because the link has a bandwidth of 2 Mbps so even if they transmit at the same time they will not exceed the bandwidth. However, if three or more users transmit at 1 Mbps at the same time, then there will be a queuing delay because the 2 Mbps link will not have enough bandwidth to support all the users transmitting at the same time. 
    
c. Find the probability that a given user is transmitting.

0.20 (20%)
    
d. Suppose now there are three users. Find the probability that at any given
time, all three users are transmitting simultaneously. Find the fraction of
time during which the queue grows.

The probability that the three users are transmitting simultaneously at any given time is 0.008. The queue would grow at the same rate because it would only grow when all three are running at the same time. That fraction is 1/125. 

**R14 - Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?**

ISPs at the same level often peer with each other in order to reduce costs. An IXP reduces a content provider's payments to upper-tier ISPs and gives the content provider greater control of how its services are delivered to the user. 

**R18 - How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 * 10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?**

Transmission delay is L/R and propogation delay is d/s. Queuing delay is part of the total delay as well but does not play a role in this problem because we are only working with one packet. 
T = L/R + d/s
It does depend on packet length and transmission rate. 
T = 1000 bytes/2*10^6 bytes per second + 2500000 m/2.5*10^8 m per second = 0.0105 seconds

**R19 - Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.**
a. Assuming no other traffic in the network, what is the throughput for the file transfer?

The throughput for the file transfer is limited by the link with the smallest rate, so the throughput will be 500 kbps. 

b. Suppose the file is 4 million bytes. Dividing the file size by the through-put, roughly how long will it take to transfer the file to Host B?

4*10^6 bytes / 500*10^3 bytes per second = 8 seconds

c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.

a. Now the throughput will be 100 kbps because R2 is the link with the smallest link. 
b. 4*10^6 bytes / 100*10^3 bytes per second = 40 seconds

