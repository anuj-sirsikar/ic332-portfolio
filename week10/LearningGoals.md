#### Anuj Sirsikar  

## Learning Goals  
### I can explain what autonomous systems are and their significance to the Internet.  
An autonomous system is just a group of routers under the same admimistrative control. Its significance is that by organizing routers into autonomous systems, the problems of needing every router to be able to store the inforation of communicating and routing information for every other router and not allowing admimistrative autonomy (allowing an organization to operate and administer its network while being able to connect it to other networks). Autonomous systems do not require a router in one AS to know how to get to every router on a different AS. One AS need only know how to get to a different AS.  
  
### I can describe how the BGP protocol works as well as why and where it is used.  
BGP is a inter-AS routing protocol. Packets are routed to prefixes and BGP allows each subnet to advertise their existence. Routers will find the best path to prefixes. Information is sent over TCP. BGP is extremely important because it allows autonomous systems to exist because it allows for inter-AS routing. BGP finds the best route between AS's among other things likes discovering network connection changes and adding a layer of network scurity. BGP wokrs by discovering the shortest route and storing that information. There is external BGP and internal BGP. New routes learned from eBGP by a router are readvertised to all of that router's peers. With iBGP, the new routes are only advertised to the external peers. BGP is also what allows private networks to connect to the interntet. Sources: textbook and [website](https://aws.amazon.com/what-is/border-gateway-protocol/#:~:text=Border%20Gateway%20Protocol%20(BGP)%20is,%2C%20devices%2C%20and%20communication%20technologies.). 

### I can explain what the ICMP protocol is used for, with concrete examples.  
Used by hosts and routers to communicate network-layer information to each other. Some example include error reporting, ping program, source quench message for congestion control, Traceroute, and port unreachable messages. 
