#### Anuj Sirsikar  
Received help from: Checked over and filled in gaps in my answers with those provided by Owen Pitchford.  
  
## Review Questions  
* **R11. How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?**  
NEXT-HOP: BGP uses it when making its forwarding table because it uses its IP address for the AS-PATH attribute.  
AS-PATH: BGP uses it when deciding which route to take when presented with multiple paths. It is also used to detect and prevent looping advertisements.  
   
* **R13. True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.**  
False, if a router in one autonomous system (AS) finds out about a path to another AS, it can be a part of that path and store that information itself so it can use it without having to broadcast or advertise that information to its neighbors.  

* **R19. Name four different types of ICMP messages and describe what they are used for.**  
ICMP type 0 -> code: 0 -> echo reply  
ICMP type 3 -> code: 0 -> destination network unreachable  
ICMP type 3 -> code: 3 -> destination port unreachable  
ICMP type 12 -> code: 0 -> IP header bad  
  
* **R20. What two types of ICMP messages are received at the sending host executing the *Traceroute* program?**  
Type 3 -> code 3 & Type 11 -> code 0  
  
* **P14. Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.**  
 * **a. Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?**  
 eBGP  
 * **b. Router 3a learns about x from which routing protocol?**  
 iBGP  
 * **c. Router 1c learns about x from which routing protocol?**  
 eBGP  
 * **d. Router 1d learns about x from which routing protocol?**  
 iBGP  
  
* **P15. Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.**  
 * **a. Will I be equal to I1 or I2 for this entry? Explain why in one sentence.**  
 I1, because it travels in the direction of the path with the least cost to the gateway router.  
 * **b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I1 or I2? Explain why in one sentence.**  
 I2, because it starts the shortest path to the gateway router.  
 * **c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I1 or I2? Explain why in one sentence.**  
 I1, because that path has the shortest AS-PATH.  
  
**P19. In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?**  
A-B -> A-W and A-V AS-PATHs  
A-C -> A-V AS-PATH  
AS routes C receives: A-V, B-A-W, B-A-V  
  
* **P20. Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​**  
No, because Z will transmit Y's traffic and because X and Y have a peering agreement, Y can re-advertise Z's routes to X. This allows X to transmit its traffic through Y and Z cannot do anything to prevent it from happening.  