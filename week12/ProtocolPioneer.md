#### Anuj Sirsikar  

## Introduction  
This is an interactive story lab which served as a beta tester with the code being in Python. 
## Collaboration  
Worked with Owen Pitchford and Ryan Schleicher. We also looked at Alex Traynor and Gabe Spencer's work when we got stuck and were having trouble. 
## Process  
Followed instructions for act II chapter 1 from [this](https://gitlab.com/jldowns-usna/protocol-pioneer) gitlab.  
## Questions  
* ### Include your client and server strategy code.
```
x = math.floor((self.current_tick()%400)/100) + 1
# and the addition of the conditional statement 'if str(x) == self.id:'
if not self.interface_sending(selected_interface):
        if str(x) == self.id:
            if len(self.from_layer_3()) > 0:
                msg_text = self.from_layer_3().pop()
                print(f"{self.id}: Attempting send.")
                self.send_message(msg_text, selected_interface)
```
* ### Explain your strategies in English.
Our first strategy was to split up the current ticks and assigned a number to each client and server and depending on the value of the current tick, a certain client/server would be able to transmit. It made sense to us in theory, but our steady-state suc* ### Any other comments?cess rate was extremely low. Then, we looked at Alex and Gabe's approach and made our own spin on it with different numbers. We chose 400 to mod the current ticks by because we tried out some other numbers first and we got the best results with 400. We thought why not just mod by 4 but we needed to account for enough time for a message to transmit and we figured giving more time would be better.  

* ### What was your maximum steady-state success rate (after 300 or so ticks?)
0.196 however we hit 0.201 at around 200 ticks.  

* ### Evaluate your strategy. Is it good? Why or why not?
First strategy was bad, but second one worked well because the steady-state success rate was pretty high.  

* ### Are there any strategies you'd like to implement, but you don't know how?
We would like to implement slotted ALOHA but we did not know how to. We would also like to find a solution that doesn't just feel like guess and check.  



