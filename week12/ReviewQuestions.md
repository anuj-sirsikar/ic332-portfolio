#### Anuj Sirsikar  

## Review Questions  

### R4. Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as dprop. Will there be a collision if dprop < L/R? Why or why not?  
If the propagation delay between the two nodes is less than L/R (transmission delay), then a collision will occur because one node will still be transmitting while it begins to receive the packet from the other node.  
  
### R5. In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?  
* *Slotted ALOHA* transmits continuously at the full rate when that node is the only active node, is highly decentralized because each node detects collisions and decides when to retransmit on its own (but it is not fully decentralized because all nodes are centralized). ALOHA is also a very simple protocol.  
* In *token passing* if a node has frames to transmit when it receives the token, then it sends up to a max number of frames before forwarding the token to the next node. Token passing is also decentralized (there is no master node) and it is highly efficient.  
  
### R6. In CSMA/CD, after the fifth collision, what is the probability that a node chooses K = 4? The result K = 4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?  
* probability that a node chooses K=4 out of {0,1,...,30,31} is 1/32.  
* K = 4 corresponds to a 4*512 = 2048 bits. On a 10 Mbps Ethernet it will be 204.8 microseconds.  
  
### P1. Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.  
```
1110 | 1  
0110 | 0  
1001 | 0  
0101 | 0  
- - -   
0100   1  
```  
  
### P3. Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.  
```
In + te + rn + et and then take the 1s complement of that 

    01001001 01101110  (Tn)
+   01110100 01100101  (te)
----------------------
    10111101 11010011
+   01110010 01101110  (rn)
----------------------
    00110000 01000010
+   01100101 01110100  (et)
----------------------
    10010101 10110110 

1s complement -> 01101010 01001001 = checksum  
```  
  
### p6. Consider the previous problem, but suppose that D has the value  
* **a. 1000100101.**  
R = 10001  
  
* **b. 0101101010.**  
R = 1  
  
* **c. 0110100011.**  
R = 01  
  
### P11. Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
* **a. What is the probability that node A succeeds for the first time in slot 4?**  
N = 4  
Looking at first success so p = p(1-p)^3  
4(p(1-p)^3)(1-(p(1-p)^3))  
  
* **b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?**  
N = 4  
4p(1-p)^3  
  
* **c. What is the probability that the first success occurs in slot 4?**  
three failures before the first success  
(3(1-4p(1-p)^3)) * (4p(1-p)^3)  
  
* **d. What is the efficiency of this four-node system?**  
4p(1-p)^3 because effeiciency is the probability that a node will sucessfully be slotted  which is Np(1-p)^(N-1)
  
### P13. Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is dpoll. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?  
Max throughput is NQ/N(Q/(R+dpoll))  