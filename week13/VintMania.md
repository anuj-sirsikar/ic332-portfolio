#### Anuj Sirsikar  

### Vint Cerf Facts That Will Knock Your Socks Off  
1. He is currently working on Interplanetary Internet with NASA which is a computer network in Space.  
2. He worked on the APOLLO program while in high school.  
3. He wrote the first TCP protocol in 1974.  
  
### Questions for Vint  
1. What are the advantages and drawbacks to technological advancements being driven by the private sector instead of the military and government?  
 * This one is interesting to me because Vint started his career developing the Internet for the military and government, but then went on to advocate to make it available to the public. Nowawadys, the cutting edge technology is being developed in the private sector and the military is behind on its technologial capabilities. I think it would be interesting to ask him this question because he worked on cutting edge technology when the military was in charge of it and he has also been a vice president at Google and knows all the advancements going on there. I think he would have a unique perspective that would allow him to provide a nuanced answer to this question.  

2. How is the Interplanetary Internet system being used today and what are some of its potential uses in the coming future?  
 * The Interplanetary Internet system in general seems really cool. I couldn't really find too much baout how it is being now, so I am curious if it still is being used. And if it is, how is NASA using it and also how do they plan on expanding its capabilities in the future. The space and the Internet both interest me and their crossroads is really cool. I am excited to learn more.  