#### Anuj Sirsikar  
  
## Review Questions  
* **R20. In the TLS record, there is a field for TLS sequence numbers. True or false?**  
False. Only fields are type, version, length, data, and HMAC.  
  
* **R21. What is the purpose of the random nonces in the TLS handshake?**  
Nonces defend against "connection replay attacks" while sequence numbers defend against replaying individual packets during an ongoing session. So say Jaquinias (the attacker) gains access to all the messages Darnell sends Mike, and the next day Jaquinias sends that same sequence of messages to Mike pretending to be Darnell. If Mike doesn't use nonces, he will think he still talking to Darnell and Mike wil think all is good. However, by Mike useing nonces for each TCP session it makes the encryption keys be different for each TCP connection. This way Jaquinias cannot pretend to be Darnell and use the same sequence of messages and get away with it because Jaquinias' messages will fail the integrity checks.  
  
* **R22. Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.**  
True. This is step 4 of the TLS handshake and the server does send the client the IV (initialization vector) in the clear.  
  
* **R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?**  
Step 6 of the TLS handshake when the server side person so Trudy sends the HMAC messages from the handshake and Bob sees the inconsistences because Trudy of cource doesn't have Alice's private key.  
  
* **P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys, SA and SB, respectively. Alice then computes her public key, TA, by raising g to SA and then taking mod p. Bob similarly computes his own public key TB by raising g to SB and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising TB to SA and then taking mod p. Similarly, Bob calculates the shared key S′ by raising TA to SB and then taking mod p.**   
[Answers in Week 15 portfolio](https://gitlab.com/anuj-sirsikar/ic332-portfolio/-/blob/master/week15/Week_15_P9.pdf)  
  
* **P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?**  
Maybe it uses MAC addresses rather than digital signature because it takes less time to just use MAC addresses instead of having to make digital signatures and because every message's integrity has to be checked, it takes less time to use MAc address than digital signatures.  
  
* **P23. Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.**  
R2 detects the duplicate by checking the nonce and seeing that there are inconsistences in what R2 gets and what it excepts to get. R2 can also check sequence numbers, but the nonce also helps R2 detect that something fishy is going on.  

#### Feedback Link  
[feedback](https://gitlab.com/ic3223/ic322-portfolio/-/issues/27)  