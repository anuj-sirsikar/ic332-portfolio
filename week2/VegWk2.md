#### Anuj Sirsikar   m255838

## Learning Goals
**I can explain the HTTP message format, including the common fields.**
There are two types of HTTP messages: request and response. 

Request: the first line is called the request line and all the other lines are header lines (at least one line but can have many more). A request line has three fields 
which include the method field, the URL field, and HTTP version field. Each header line contains some information like "Host:" and "User-agent:". There is also an entity body below the header lines. 

Response: has three sections: initial status line, six header lines, and the entity body. Entity body holds the requested object. The status line includes the protocol version field, a status code, and a corresponding status message. The header lines include Connection, Date, Server, Last-Modified, Content-Length, and Content-Type. The status codes and their phrases tell the result of the request. 

**I can explain the difference between HTTP GET and POST requests and why you would use each.**
GET requests are used when "the browser requests an object, with the requested object identified in the URL field" (textbook). The entity body is empty 
for GET requests, however it is used with POST requests. POST requests are used when the user fills out a form like entering a password, username, address, or credit card information. 
During a POST request, the user is still requesting a webpage from the server, but the contents of the web page are dependent on what the 
user puts into the form's fields, and that information is added to the entity body which is why it isn't empty. It is important to note that just because 
a form is used, doesn't mean a POST request is sent. The form can also use a GET request and then the info from the form's fields will just be part of the 
URL. That is why POST requests are used for sensitive information so that what is entered into the fields is not part of the URL. You would not want your bank account
password be in your URL. Whereas,when you search something into a search engine, you send a GET request because that information is not sensitive and thus will be 
part of the URL for that webpage. 

**I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.**
Conditional GET requests allows a cache to verify that its objects are up to date. You don't want the copy of an object to have been modified since the copy was cached at the client thus resulting in a stale cache. Conditional GET requests are HTTP request messages that are GET requests and include an "If-Modified-Since:" header line. When a request message comes in, the conditional GET checks to see if the object's "Last-Modified" date is closer to the current date than the "If-Modified-since" date. If it is, then all good, but if not, then it sends a message that the object is not modified. 

**I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.**
Response: has three sections: initial status line, six header lines, and the entity body. Entity body holds the requested object. The status line includes the protocol version field, a status code, and a corresponding status message. The header lines include Connection, Date, Server, Last-Modified, Content-Length, and Content-Type. The status codes and their phrases tell the result of the request. They are used as responses to request messages to make sure the server got the request and returned info in response to the request and are sent by the web server where request messages are sent from the user. 
    
Common Codes:
200 OK: request suceeded and info is returned in response
301 Moved Permanently: Requested object has been permanently moved and the new URL is automatically retreived 
400 Bad Request: generic error code which indicates that the request couldn't be understood by the server
404 Not Found: requested document does not exist on this server
505 HTTP Version Not Supported: requested HTTP protocol version is not supported by the server 

**I can explain what cookies are used for and how they are implemented.**
Wesbites use cookies to keep track of users. In layman terms, or my terms, the first time you visit a website, that website's server assigns you a unique id number. At that time, the website also creates
a an entry into its back-end database that is indexed by the user's id number. The website sends your browser the id number it has assigned you, and your browser stores that id number for that particular 
website in its cookie file. So each time you visit that website in the future, your browser will retreive 
your id number for that website. This is how websites can track your activity and store your information
like name, address, and credit card information along with what you have bought in the past. This helps 
with one-click shopping for example. 

**I can explain the significance HTTP pipelining.**
It allows back-to-back requests for objects which means that requests can be sent consecutively without
waiting for replies to pending requests. This can all be done between the server and client on a single persistent TCP connection. It basically opens the door for successive requests without the need to wait 
for a responce over a single connection. 

**I can explain the significance of CDNs, and how they work.**
CDNs (Content Distribution Networks) help with distributing massive amounts of web content to people
all over the world. A CDN will manage servers in different locations all around the world which will have 
copies of whatever web content is being shared and the CDN will connect each user request to a CDN location that will give the user the best experience. There are private CDNs owned by content providers (i.e. Google)
and also third-party CDNs that distribute content for multiple providers. CDNs usually add videos (make a copy of them) once it has been requested by a user and if the space fills up then it gets rid of the once least used. When a browser is told to retrieve a video by the user, the CDN has to intercept the request to 
find out which CDN cluster will be best suited for the client and redirect the request to a server in that cluster. 

## Textbook Problems 
**R5 - What information is used by a process running on one host to identify a process running on another host?**
An IP address is used to identify a host and a port number is used to identify the receiving process running in the host. 

**R8 - List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.**
Reliable data transfer - TCP
Throughput - UDP
Timing - UDP
Security - TCP

**R11 - Why do HTTP, SMTP, and IMAP run on top of TCP rather than on UDP?**
Because HTTP, SMTP, and IMAP need to prioritize a reliable data tranfers and security over speed. 

**R12 - Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.** 
Each time a new customer accesses the website, the website will assign that user a specific id number. The website will send that number to customer's browser which 
will store in the information in its cookies file. Each time an existing logs in, that customer's browser will access its id number for that website and send it to the 
website and then the website will know which user is on the site by the user's id number. Based on customer preference, the website can store the customer's name, address, 
credit card information so that all that is already in the system waiting for the user to buy something. The website can also use cookies to see what the customer bought
in the past and use that information to recommend additional purchases. 

**R13 - Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?**
A Web cache is basically a proxy server that stores content locally. When a user inputs a HTTP request, the browser first establishes a connection with the web cache to see if the content is stored their locally first before having to get it from the main server. This can reduce the delay because if the object is stored locally on the web cache, then it will be very quick to receive the requested the object. But it will not reduce the delay for all objects, it will only do so for the objects that are on
the Web cache (usually stuff requested frequently by multiple users). 

**R14 - Telnet into a Web server and send a multiline request message. Include in the request message the 'If-modified-since:' header line to force a response message with the '304 Not Modified' status code.**
[Not sure how to do this one, need to go over in class]

**R26 - In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connections, each from a different client host, how many sockets would the TCP server need?**
UDP only needs one socket because it focuses on being quick and in order to facilitate more real-time work. TCP needs two sockets because it establishes a connection going both way which is why TCP is more secure and reliable than UDP, but that means it is slower and not as good for real-time stuff. TCP would need 2n sockets. 
