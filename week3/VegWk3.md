#### Anuj Sirsikar   m255838

## Learning Goals 
**I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.**
Main purpose of DNS is to translate host names to IP addresses. 

Root: More than 1000 globaly but they are all copies of 13 different root servers managed by 12 organizations. Root name servers provide the IP addresses of the TLD servers. 

Top-level domain (TLD): Top-level domains include com, org, edu, gov, net, etc... The network infrastructure that supports these domains is very complex. TLD servers provide the IP addresses for authoritative DNS servers. 

Authoritative: Every organization with publicly accessible web and mail servers has to publicly document their DNS records that include the names of those hosts (servers) and their associated IP addresses. An organization's authoritative server holds that information. 

Local: Each ISP has a local DNS server. "When a host connects to an ISP, the ISP provides the host with the IP address of one or more of its local DNS servers" (textbook). 

When a host makes a DNS query, the query message (containing the hostname to be translated) goes to its local server. The local server first sends it to the root server and the root server takes note of the TLD domain (i.e. edu) that the host is trying to reach. The root server returns to the local server all the IP addresses for TLD servers of that TLD domain. Then the local server sends the message to the particular TLD server (form the domain). The TLD server sees the next part of the suffix and responds with the IP address of the authoritative server that matches the suffix (i.e. umass.edu). Then the local server sends the message directly to the authoritative server (i.e. dns.umass.edu) which responds with the IP address of the host it was originally trying to reach (i.e. gaia.cs.umass.edu). With DNS caching, the information doesn't have to go through the local server each time because the information is stored on local memory. 

**I can explain the role of each DNS record type.**
[most of these answers come word for word from the textbook although I tried to simplyfy it and put it into my own words as best as I could]
A - provides hostname to IP address mapping. example-> (relay1.bar.foo.com, 145.37.93.126, A) 
NS - used to route DNS queries further along. Contains the domain name and name of authoritative server.
example-> (foo.com, dns.foo.com, NS)
CNAME - provides querying hosts the canonical (nickname or alias) name for a hostname. Contains a canonical hostname for the hostname. exampe-> (foo.com, relay1.bar.foo.com, CNAME)
MX - allows hostnames of mail servers to have simple aliases. This way, a company can have the same 
aliased name for its mail server and other servers. Includes the canonical name for the mail
server and the hostname. example-> (foo.com, mail.bar.foo.com, MX)

**I can explain the role of the SMTP, IMAP, and POP protocols in the email system.**
SMTP - principle application-layer protocol for email. Uses TCP (reliable) to transfer emails between 
mail servers. Includes a client and server side. Very old though and so is a bit archaic in 
nature. SMTP facilitates the transfer of email over the interenet over reliable TCP and has clients and servers introduce themselves to each other before starting. Uses persistent connections. 
IMAP - alternative method to retreive an email from a mail server. Allows the client to manage folders 
and messages in their web server
POP - "used to retrieve e-mail from an e-mail server for the client application" (https://www.sciencedirect.com/topics/computer-science/post-office-protocol#:~:text=Preparation%20Kit%2C%202009-,POP,each%20e%2Dmail%20account%20name ). 
    
**I know what each of the following tools are used for: nslookup, dig, whois.**
nslookup - used when the user would like to directly send a DNS query message from the host they are on to some other DNS server. You can see the reply message 
from the DNS server and you can specify what type of server you want to send a message to (root, TLD, or authoritative)
dig - used for uncovering DNS name servers thus it is used for toubleshooting DNS problems. It can perform DNS lookups and display answers. It can be used from 
command line and also can be used for reading lookup requests from a file (https://www.ibm.com/docs/en/aix/7.1?topic=d-dig-command).
whois - used for tracing the ownership and tenure of domain names (https://www.whois.com/whois/)

## Textbook Problems 
**R16 - Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.**
This process will use SMTP. Alice will open up gmail and type up and send an email to Bob's email address. Gmail will send the email to her mail server where it will be in a message queue. The client side of SMTP on Alice's mail server will see the email in the message queue. It will then open up a TCP connection to an SMTP server on Bob's mail server. There will be some SMTP handshaking and then when all is well the client SMTP will send Alice's email into the TCP connection. The server side of SMTP at Bob's mail server will receive the emailand puts it in his inbox via HTTP. Then Bob can log into his email account to read the email. 

**R18 - What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?**
The HOL blocking issue in HTTP/1.1 is that because it uses persistent TCP connections it is possible that a webpage only needed one TCP connection. However, implementing only one connection resulted in head of line (hol) blocking and bottlenecking because a large object near the top of a webpage would hold up the smaller objects below it making webpage run very slowly. To solve this, HTTP/1/1 browsers would open many TCP connections in parallel in order to prevent HOL blocking and increase bandwidth. HTTP/2 solves this by breaking each message into small frame and keeping the request and response messages on the same TCP connection. It sends the a fixed amount of frames over the TCP connection so it would break up messages of very large frames. That way it doesn't have to open up parallel connections. 

**R24 - CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.**
Enter Deep - putting server clusters in access ISPs all over the world in order to get close to the end users. This will improve user-perceived delay by decreasing links and routers between end users and CDN server. 

Bring Home - putting large clusters at a smaller number of places so CDNs put their clusters in IXPs. Results in lower maintenance and management but at the cost of higher delay and lower throughput to end users. 

**P16 - How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.**
SMTP marks the end of a message body with a line that only contains a period while HTTP uses a content-length header field to display the length of a message body. No, because HTTP message could be in binary data, but the SMTP message body must be in 7 bit ASCII format. (https://wiki.eecs.yorku.ca/course_archive/2012-13/W/3214/_media/solutions_to_chapter_2_problems.pdf)

**P18 -**
**a.What is a whois database?**
Database that holds information about when someone registers a domain name or updates their DNS settings (https://www.domain.com/blog/what-is-whois-and-how-is-it-used/#:~:text=WHOIS%20is%20a%20public%20database,days%20of%20the%20early%20Internet). 

**b. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used**
I used whois.com to obtain two DNS servers under the domain Google.com: ns2.google.com and ns1.google.com

**P20 - Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.**
I would check the chaches in the local DNS servers because the most popular web servers will be visited most often which means they will be most likely store in the cache so that the local DNS server doesn't have to keep trying to connect with some other website that it connects to everyday. 

**21 -  Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.**
(I'm not sure about this... go over in class)
Thinking out loud-> As an ordinary user I don't think you would have access to that information striaght up so you would not be able to determine. 


