#### Anuj Sirsikar  m255838

## WireShark DNS Lab

# Introduction:
This lab involved learning about how DNS messages are sent and the effects of DNS caching. The lab also involves using tools like nslookup in order to find different IP addresses. I also learned fist hand how the root, TLD, and authoritative servers work together to connect two different servers. 

# Collaboration:

# Process: 
http://www-net.cs.umass.edu/wireshark-labs/Wireshark_DNS_v8.1.doc

# Questions:
**1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in.  What is the IP address of www.iitb.ac.in**
103.21.124.10

**2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?**
127.0.0.53 (when ssh into usna lab computer)
10.1.74.10 (on my windows machine)

**3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?**
Non-authoritative server

**4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain.  What is that name?  (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?**
dns3.iitb.ac.in
I did "nslookup dns3.iitb.ac.in" to find the IP address of that authoritative name server
which is 103.21.127.129

**5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message?  Is this query message sent over UDP or TCP?**
The packet number is 96 and the query message is sent over UDP (user datagram protocol).

**6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message?  Is this response message received via UDP or TCP?**
Packet number is 107 and the message is received via UDP. 

**7. What is the destination port for the DNS query message? What is the source port of the DNS response message?**
Destination port: 53
Source port for DNS response: 53

**8. To what IP address is the DNS query message sent?**
10.1.74.10

**9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?**
1 question and 0 answers

**10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?**
1 question and 0 answers

**11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu.**
**What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/?**
179
**What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address?** 
204  
**What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg?**
51
**What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.**
80
DNS caching affects the answer because if the cache was not just cleared then it would not have to go get the image object and it would already have it locally 
because it is a request that is made everytime. 

**12. What is the destination port for the DNS query message? What is the source port of the DNS response message?**
Destination port: 53
Source port of response: 53

**13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?**
10.1.74.10, yes it is the IP address of my default local DNS server.

**14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?**
Standard query and no answers. 

**15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?**
Standard query response with 1 question and 0 answers. 

**16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?**
10.1.74.10, yes it is the IP address of my default local DNS server.

**17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?**
1 question and 0 answers.

**18. Examine the DNS response message.  How many answers does the response have?  What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?**
0 answers. 















