#### Anuj Sirsikar  m255838

## TCP Reset and TCP Reset Attacks 
* [TCP Reset link](https://www.extrahop.com/company/blog/2021/tcp-resets-attack-or-defender-containment/)  
* [TCP Reset Attacks link](https://www.wallarm.com/what/what-is-syn-spoofing-or-tcp-reset-attack)  

Learning about how packets are sent between hosts over a TCP connection made me ponder how such a connection could compromise a host   
if the sender was sending a malicious packet or if the connection was somehow not consensual or if the sender sent a packet that was  
not agreed upon. I thought about how the connection could be exploited by packets that want to cause harm to the receiving host and  
what procedures are in place to prevent this from happening.  
  
As it turns out, there are attackers out there that try to compromise hosts on networks in order to set up a command and control  
server at that compromised host in order to aquire footing within the network and then they can reak havoc within the network itself  
by sending commands to the compromised host. The TCP reset functionality was set in place to stop this to some extent, however, TCP  
reset ends the connection and has the two servers start over. This function can prevent attackers from inflicting harm on other hosts  
and networks, but TCP reset was set in place more for the purpose that if one of the machines shut down or if something between the  
two hosts goes wrong then the connection can immediately be ended and the two servers can start the connection and packet transfers  
over again. For this reason, attackers can also exploit TCP resets.  
  
The basic idea of a TCP reset attack is the attacker sending fake TSP reset packets to the host. The difference here is that the sender  
is sending the TCP reset packet rather than the receiving host. If the receiving host were sending the TCP reset, then it would prevent  
the attacker from carrying out their malicious intentions. However, in a TCP reset attack the sender will send lots of packets to the  
receiving host and overwhelm it so it becomes very busy and starts to drop packets. The point being that the attacker wants the receiver  
to stop listening for incoming packets from the attacker. When this happens, the atacker will send a false TCP reset packet. If the   
receiving host was listening to the attacker, then the attacker would not send the false TCP reset at that time. This is a form of  
denial of access attack because what happens is the receiving host does not think that the TCP reset packet is coming from the attacker  
because it is not listening to the attacker. So the host closes its TCP connection without whatever other host it is listening to and then  
the attacker will send another false TCP reset packet which will close another connection at the receiving host and it will keep on doing  
this until the receiving machine is totally shutdown and cannot be accessed by other hosts.  

Two more common ways to mitigate such an attack include using SYN cookies and RST cookies. The point of this two tools is to check if the sender  
is valid. SYN cookies will send a SYN-ACK with a cookie back to the sender and then if the receiver gets an ACK with the cookies back, then the  
the receiving host knows that the sender is valid and exists. It is pretty much the same idea with RST cookies. This time there is a server (bodyguard)  
that is before the main receiving server. So the sender sends a packet and then the bodyguard server will send an invalid ACK back to the sender.  
If at that point, the sender sends a TCP reset packet, then the bodyguard server knows that the sender is valid and when the sender resends the original  
packet, the bodyguard will send it through to the intended receiving host.  
  
I hope you enjoyed this short essay and learned something new!  