#### Anuj Sirsikar  m255838

## Learning Goals 
**I can explain how TCP and UDP multiplex messages between processes using sockets.**
Multiplexing: extend host-to-host delivery provided by network layer to a process-to-process delivery service for applications running on the hosts. 

UDP socket - destination IP, destination port (two-tuple)
UDP will associate a port number to a socket so that data from multiple different sources going to the same port will all go to the same socket. 

TCP socket - source IP, source port, destination IP, destination port (four-tuple)
TCP will associate a port number to seperate sockets. A new socket is made after every new connection. 
    
**I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.**
TCP - protocol, reliable, connection-oriented (must handshake); TCP endpoint open up in each host and then there is a protocol between them; connection is not "end-to-end"; 

UDP - not as reliable and secure as TCP, better for videos where losing a couple frames won't make too much of a difference; segments could be lost or delivered out of order, no handshaking needed, can function when network is compromised, 

**I can explain how and why the following mechanisms are used (and which are used in TCP): sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.**
sequence numbers - number that is the first byte in the segment's data (used in TCP)
    
duplicate ACKs - prevents loss events when transmitting messages between hosts; it lets the sending host know that the receiving host has received the sequence and that it has been corrupted because the same ACK has been sent to the sending host twice (and that ACK that has been sent twice is the last good ACK which removes the need for sending a NACK because a duplicate ACK is an implied NACK). If pipelining between hosts, the receiving host will send an ACK for each sequence it receives. TCP solves if the first sequence is corrupted by the three way handshake. 

timers - used to make sure that there are no super long delays in a TCP connection (https://www.geeksforgeeks.org/tcp-timers/); also used to determine when to re-tranmit a packet if the sender has not yet received an ACK. 

pipeling - can send multiple packets at once without waiting for one to go all the way through and without having to wait for a response 

go-back-N - a generic form of pipelines protocols (for error recovery), allocates spaces up to N of consecutive transmitted but unACKed packets; receiver sends the acknowledgement back to the sender, if it gets them out of order, then it will send back the ACK for the last good sequence and then drop the packet that it got out of order. Can transmit multiple packets without ACK for each packet.

selective repeat - other generic form of pipelines protocols(for error recovery); receiever individually acknowledges whether or not each packet is correct received and it allows a TCP receiver to individually and selectively acknowledge out-of-order segments. For out of order sequences, it will send the ACK for the out of order sequence and then it will buffer and wait for the sender to send the correct packet again. Timer for each sequence. 

sliding window - used to find out the number of unacknowledged bytes that one host can send to another; used by TCP for flow control (https://www.ibm.com/docs/en/spectrum-protect/8.1.11?topic=tuning-tcp-flow-control)


**I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.**
Go-back-n (gbn) and selective repeat (sr) both have their own ways for solving packet issues. gbn will drop packets that are received out of order whereas sr will buffer the packets until the packet is resent for example. Timers are used by the sender to know when a packet has been lost so that the sender has a given amount of time where if they don't receive an ACK, then they know to resend the packet. A big difference between Gbn and sr is that gbn will resend all the packets whereas sr will only re-tranmit the packet that was lost or damaged. Both protocols use sequence numbers, along with timers, to solve these problems and usually the solution involves identifying whether a packet has been lost or dropped or curropted or is out of order and having the receiver send that information to the sender and the sender will then re-transmit the packet. 
  
## Textbook Problems 
**R5.  Why is it that voice and video traffic is often sent over TCP rather than UDP in today’s Internet? (Hint: The answer we are looking for has nothing to do with TCP’s congestion-control mechanism.)**
Voice and video traffic is often sent over TCP rather than UDP in today's internet because first of all some organizations block UDP traffic for security purposes, but also UDP doesn't have congestion control so TCP is a better option when people are streaming a lot. Also with low packet loss rates, TCP is a better option. 

**R7.  Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?**
Both host a and b will be directed to the same socket at host c. The source port number for the a-c segment and the b-c segment is how host c knows that those two segments originated from two different hosts (because the source post numbers are different).

**R8.  Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.**
The Web server will make a new process for each connection and even though both requests have port 80, they are being sent through different sockets. Each connection has its own socket through which HTTP requests and responses are received and sent. 

**R9. In our rdt protocols, why did we need to introduce sequence numbers?**
Because the sender can send duplicate packets (it will resend a packet if it doesn't get a positive ACK in time), and the receiver needs to be able to differentiate between a packet with new data and a retransmission of a previous packet that wasn't received properly. 

**R10.  In our rdt protocols, why did we need to introduce timers?**
RDT protocols need timers because otherwise the sender will not know how long to wait before declaring that a packet has been lost. A timer starts each time the sender transmits a packet and if the sender does not receive an ACK in the alloted time and the timer runs out, then the sender will know that something went wrong and at that time the sender will re-transmit the packet. 

**R11. Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain.**
A timer is still necessary because the packet could be lost and then a timer would be needed to know when to resend the packet. The ACK could be lost which would also trigger a timeout if there is a timer, so a timer is needed there to know when to resend the packet. 

**R15. Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.**
a. How much data is in the first segment? -> 20 bytes 
b. Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number? -> ACK 90 (previous ACK is resent to sender)

**P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?**
sum: 01010011+01100110 = 10111001 -> 10111001+01110100 = 00101100 -> 1s complement = 11010011; UDP takes the 1s complement of the sum instead of just the sum in order to easily see if there were errors introduced into the packet. Errors are detected if the sum at the receiver is not comprised of only 1s or if one of the bits is 0. UDP has this in place because no where else in UDP is there a guarentee of error checking. This is the one place in UDP where error checking is guarenteed even though UDP won't do anything to address the error. One bit error will not go undetected but a 2 bit error can go undetected (not actually sure about that last sentence). 

