#### Anuj Sirsikar  m255838  

## Homework Questions  
**R17. Suppose two TCP connections are present over some bottleneck link of rate R bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?**  
R/2  
**R18. True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of ssthresh is set to one half of its previous value.**  
false  
**P27. Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.**  
* **a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?**  
sequence number = 207  
source port number = 302  
destination port number = 80  
* **b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?**  
acknowledgement number = 207  
source port number = 302  
destination port number = 80  
* **c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?**  
acknowledgement number would be 127 to show that it received the packets out of order with the last good byte being 127. 
* **d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledg-ment that you add, provide the acknowledgment number.**  
I'm just going to explain what happens:  
 * seq 127 with 80 bytes is sent and then seq 207 with 40 bytes is sent.  
 * ack 207 doesn't make it back to the sender but ack 247 does (ack for the second segment)  
 * when the timer for the first segment times out because it has not received an ack, it will send seq 127 again
 * while this is going on, seq 207 is buffered at the receiver end, so once it receives seq 127 again, it will send ack 247 back to the sender again.  
**P33. In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?**  
TCP avoids measuring SampleRTT for retransmitted segments because it wouldn't be able to  
differentiate between a late ACK coming from the originally transmitted segment and an ACK  
for the retransmitted segment and could therefore get the round trip time wrong for the  
retransmitted segments.  
**P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?**  
[Quick refresher on retransmit vs fast retransmit](https://wiki.wireshark.org/TCP_Analyze_Sequence_Numbers#:~:text=TCP%20Retransmission%20%2D%20Occurs%20when%20the,expiration%20of%20the%20acknowledgement%20timer.)  
Becuase say multiple packets are sent in a short time interval or even at the same time and they are receievd out of order. One or maybe even two duplicate ACKs would be sent at first because of the out of orderness but due to buffering, the receiever can see what it has as the orginial (out of order) packets start to come in and so the designers probably did not want to perform a fast retransmit unless the receiever actually did not receieve the packets at all oppossed to just receiving them out of order.  
**P40. Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.**    
* **a. Identify the intervals of time when TCP slow start is operating.**  
[1,6] and [23,26]  
* **b. Identify the intervals of time when TCP congestion avoidance is operating.**  
[6,16] and [17,22]  
* **c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?**  
triple duplicate ACK  
* **d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?**  
timeout  
* **e. What is the initial value of ssthresh at the first transmission round?**  
32  
* **f. What is the value of ssthresh at the 18th transmission round?**  
21  
* **g. What is the value of ssthresh at the 24th transmission round?**  
14.5  
* **h. During what transmission round is the 70th segment sent?**  
I'm not really sure  
* **i. Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ssthresh?**  
I'm not really sure  
* **j. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 16th round. What are the ssthresh and the congestion window size at the 19th round?**  
Same as above  
* **k. Again suppose TCP Tahoe is used, and there is a timeout event at 22nd round. How many packets have been sent out from 17th round till 22nd round, inclusive?**  
Not really sure -> need to go over 40h - 40k. Reading this [link](https://www.geeksforgeeks.org/tcp-tahoe-and-tcp-reno/) to help me.   