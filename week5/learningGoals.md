#### Anuj Sirsikar  m255838  
  
## Learning Goals  
**1. I can explain the problem that TCP congestion control is solving for, and how it solves that problem.**  
Congestion control prevents any one connection from hogging up all the resources from other communicating hosts by running extensive traffic over that one connection. Wants to give each connection and equal amount of bandwidth. TCP carries this out by regulating the rate of traffic the sender can send over a TCP connection into the network. There is end-to-end and network-assisted congestion control. If there is little congestion, the sender will increase the send rate and if there is congestion, then the sender will decrease the send rate. Congestion control mechanism at the sender keeps track of the variable, congestion window (cwnd). Congestion window contains the value which is the rate at which a TCP sender can send traffic into the network. Lost segment implies congestion and the rate will be slowed down, whereas an ACK means no congestion so the rate can be sped up.  
congestion control algorithm has 3 main parts:
 1. slow start - when TCP connection begins, cwnd is typically the small value of 1 MSS  
 2. congestion avoidance - cwnd is half its value when congestion was last encountered  
 3. fast recovery - cwnd is increased by 1 MSS everytime duplicate ACK is received for TCP to enter fast recovery state  

**2. I can explain the problem that TCP flow control is solving for, and how it solves that problem.**  
TCP flow control is solving for the send overflowing the receiver's buffer which could lead to dropping packets. The rate of flow from the sender is controlled by the receiver. Sender maintains variable called the receive window which tells the sender how much buffer space is still available at the receiver. Both the sender and receiver have this variable and everytime the sender sends a message to the receiver, the receiver will send back with the ACK the receive window value to tell the sender how much space is left in its buffer. 