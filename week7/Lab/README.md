**How to use router-simulator.py**  
* type "python3 router-simulator.py" into command terminal  
* type in a 32-bit binary IP address  
* hit 'enter'  
* the code will output the interface to forward the packet based on the address in the router's routing table, and otherwise will return "Fa 3/1" if it gets no matches and thus will forward on that interface  