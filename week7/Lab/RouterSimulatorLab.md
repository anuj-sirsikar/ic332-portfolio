#### Anuj Sirsikar  m255838  

## Introduction  
The premise of this labratory excercise involves acting as a router which must forward an incoming binary address to the appropriate interface. The code will take in a single 32-bit binary value from stdin which will act as an IP address. Then the code which is the acting as a router will return the interface that matches the given IP address to the largest bit based on the IP addresses in its routing table. Code mostly works as expected but I think there is one case that should output Fa 2/7 but instead it outputs Fa 2/5. I will continue to debug. 
## Collaboration  
In class I talked to Gabe Spencer about how to attack the problem but I didn't finish in class so the rest was on my own. I also had to look up python syntax a good amount because I am still learning.  
## Process  
Followed the instructions on the [course website](https://courses.cs.usna.edu/IC322/#/assignments/week07/lab-router-simulator/?id=resources). Also, the source code is in this same "Lab" sub-directory.  