#Router Simulator Lab 
#Anuj Sirsikar  m255838
#Gabe Spencer sat next to me so we conversed and bounced ideas off of each other 

#user input
binaryAddress = input()


#converts IP address into binary address form
def converter(ip):
    starter = ip.split("/"); 
    addressArray = starter[0].split(".")
    converted = ""
    for i in addressArray:
        temp = bin(int(i))
        temp = str(temp)
        temp = temp[2:]
        length = len(temp)
        front = ""
        if (length < 8):
            while (length < 8):
                front += '0'
                length = length + 1
        temp = front + temp
        converted += temp
    return converted

#returns true if the IP address matches the user input, false otherwise
def compare(index, user):
    ip = converter(index[0])
    helper = index[0].split("/")
    limit = helper[1]
    limit = int(limit)
    user = str(user)
    user = user[:limit+1]
    ip = ip[:limit+1]
    if (user == ip):
        return True
    return False

#Compares values in the table to the user input and returns value accordingly
def finder(table):
    largestOne = ["0.0.0.0/0", "Gi 3/1"]
    for i in table:
        if (compare(i, binaryAddress) == True):
            tempNew = i[0].split('/')
            tempOld = largestOne[0].split('/')
            tempNew = int(tempNew[1])
            tempOld = int(tempOld[1])
            if (tempNew > tempOld):
                largestOne = i
    return largestOne[1]

# "Main"
routingTable = [["1.0.0.0/8", "Fa 0/0"],
                ["1.0.100.0/24", "Fa 0/1"], 
                ["1.99.0.0/20", "Fa 2/1"],
                ["8.0.0.0/27", "Fa 0/2"],
                ["8.8.8.0/24", "Fa 2/2"], 
                ["126.2.3.0/20", "Fa 0/3"], 
                ["8.8.8.16/30", "Fa 2/3"],
                ["237.1.1.0/30", "Fa 0/4"],
                ["237.2.0.0/16", "Fa 2/4"],
                ["99.31.4.0/22", "Fa 0/5"],
                ["99.0.0.0/8", "Fa 2/5"],
                ["101.5.0.0/16", "Fa 0/6"],
                ["223.0.0.0/8", "Fa 2/6"],
                ["101.5.7.32/30", "Fa 0/7"], 
                ["99.0.0.0/20", "Fa 2/7"],
                ["101.0.0.0/8", "Gi 1/0"],
                ["101.42.3.0/24", "Gi 3/0"],
                ["223.4.0.0/16", "Gi 1/1"]]   
#note that for any other IP address, return "Gi 3/1"

print(finder(routingTable))