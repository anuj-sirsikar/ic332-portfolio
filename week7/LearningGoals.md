#### Anuj Sirsikar  m255838  

## Learning Goals  
**I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.**  
A subnet is the network that connects multiple hosts to a router. And an interface's IP address will be determined by the subnet it is connected to. Subnets have IP addresses as well except their IP addresses will usually end in 0 followed by a /# where the number at # tells you how many of the left-most bits define the subnet address. And then interfaces on the subnet will have IP addresses that differ after those first however many bits (usually first 24 out of 32 bit quantity). The slash followed by a number which looks like "/24" is known as a subnet mask.  
Longest prefix matching is a rule routers use when an address matches multiple other ones. The router will find the longest matching entry and forwards that entry's packet to the link interface  
  
**I can step though the DHCP protocol and show how it is used to assign IP addresses.**  
DHCP is an automated way for hosts to obtain IP addresses.  
DHCP is good when users are coming and going and addresses are only needed for a certain amount of time.  
Uses **UDP**  
There are four steps:  
 1. *DHCP server discover* - arriving host must find a DHCP server to connect to. DHCP sends an IP datagram with broadcast destination address (255.255.255.255) and host address (or source IP address which is 0.0.0.0) and it is sent to the link layers and broadcast to all nodes attached to the subnet.  
 2. *DHCP server offer* - DHCP server responds to client with a DHCP offer message that is also broadcast to all nodes on the subnet using the same broadcast address. Also sends an IP address lease time which is the amount of time the IP address will be valid.  
 3. *DHCP request* - client chooses from server offers and responds to its selection with a DHCP request message which relays the configuration parameters.  
 4. *DHCP ACK* - then the server responds to the request with an ACK message which confirms the requested parameters.  