#### Anuj Sirsikar  m255838

## Homework Questions  
**R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).**  
Packet loss occurs at input ports when the router's memory is filled up by packets that are buffered. And then packet loss will occur when new packets arrive because there will not be any memory to store them or old packets will be dropped to make room for the new ones. This can be elimated if the switching fabric rate (speed of packets going from input to output ports) is N times faster than the transmission rate of the packets per second where N is the number of input and output ports assuming that there are just as many input ports as output ports. This works because even if all the packets in each input port are all going to the same output port, they will have enough time to all be sent through the switch fabric before the next set of packets arrive at the input ports.  
  
**R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?**  
If packets at each of the input ports are all headed for the same output port, in the time required to send one packet onto the outgoing link, there will be new packets at that output port. However, because the outgoing link can only send one packet in a unit of time, the other packets at that output port will have to wait to be transmitted over the outgoing link. Thus they will queue into the buffer and once the buffer fills up, packets will ultimately be lost. No, this cannot be prevented by increasing the switch fabric speed because the packets will still only be able to be transmitted on the outgoing link one at a time during any unit of time. No matter how quickly/effeciently they get to the output port, they can only be transmitted out one at a time (at a given unit of time) so increasing the switch fabric speed won't prevent this form of packet loss.  
  
**R13. What is HOL blocking? Does it occur in input ports or output ports?**  
Head-Of-Line (HOL) blocking is when a queued packet is blocked by another packet at the front of its line even though the original packet has no contention to reach its output port. But the packet at the front of the line does have contention to reach its output port so it blocks all the packets behind it as well. This occurs in input ports.  
  
**R16. What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?**  
Round robin (RR) - RR scheduler alternates service among classes. It will transmit a packet from class 1 and then one from class 2 and then class 3 and on and on and then it will return to transmit the next packet from class 1. It is a work-conserving queue which means it will never let the link be idle if there are packets waiting to be transmitted. It will go through every class until it has transmitted every packet.  
Weighted fair queuing (WFQ) - it is a generalized form of RR queuing and has been widely implemented in routers. Packets are classified and put into seperate waiting areas by class. The packets are still served in a "circular" manner which means it will serve one packet from each class before transmitting the next packet of a class. It is also work-conserving. The difference between the two is that in WFQ classes can receive differing amounts of service based on their "weight". What this means is that each class is assigned a weight and for any fraction of time, each class will be able to send a the amount of service equal to that classes weight over the total weight.  
RR and WFQ will be the same if all the classes have an equal weight in WFQ.  
  
**R18. What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?**  
Time-to-live header can do this because it is in place as kind of a timer to make sure that the datagrams do not circulate forever in the network. Its value is decreased by one each time the datagram is processed by a router and then when its value is zero, a router has to drop the datagram. So, you can set the value of time-to-live to N to ensure that the packet is forwarded through no more than N routers.  
  
**R21. Do routers have IP addresses? If so, how many?**  
Yes. Routers are capable of sending and receiving IP datagrams so each router interface must have its own IP address. The IP address is associated to the interface so a router will have as many IP addresses as interfaces it has.  

**P4. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?**  
The minimal number of time slots needed is three. You can send the top x and the middle y. Then the middle x and bottom y. And then the bottom z.  
The largest number of slots for the worst case (assuming non-empty input queue is not idle) is also three. I have to send the top x in the first time slot every time and then I also have to send a y because I can't make any input queue idle while it still has stuff in it. Even if I sent the bottom y in the first time slot, in the second time slot I would have to send the middle y and the bottom z. And then I would send the middle x. So in every case, I would need three time slots to get the job done.  
  
**P5. Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.**
 * **a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .)**  
 1123 -> 1123 -> or 2311 or 1213. But basically it will send twice as many packets from class 1 as class 2 and 3 because class one has twice the weight as classes 2 and 3.  
 * **b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?**  
 It would be 112->112->211 because there are no packets from class 3. This means that the new weights are 0.67 for class 1 and 0.33 for class 2.  
  
**P8.**  
* **a. Provide a forwarding table that has five entries, uses longest prefix match-ing, and forwards packets to the correct link interfaces.**  
forwardingTable = [[224.0.0.0/10, 0],
                   [224.64.0.0/16, 1],
                   [225.127.0.0/16, 2],
                   [225.0.0.0/9, 2]
                   otherwise it forwards it to 3]  
    
* **b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:**  
Basically what happens is the router will check each IP address in its routing table with the incoming IP address and determines the appropriate interface based on which IP address matches the incoming one to the largest bit.  
  
**P9. For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.**  
| Prefix match | Interface | Destination Address Range |  
--------------------------------------------------------
| 00           | 0         | 00000000 -> 00111111      |  
| 010          | 1         | 01000000 -> 01011111      |  
| 011          | 2         | 01100000 -> 01111111      |  
| 10           | 2         | 10000000 -> 10111111      |  
| 11           | 3         | 11000000 -> 11111111      |  
--------------------------------------------------------  

**P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.**  
223.1.17.0/26 goes to subnet 1  
223.1.17.128/25 goes to subnet 2  
223.1.17.64/28 goes to subnet 3    