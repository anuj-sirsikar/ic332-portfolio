#### Anuj Sirsikar m255838  
  
## Learning Goals  
* **I can explain the problem that NAT solves, and how it solves that problem.**  
NAT solves the problem of connecting devices on private networks to the public Internet and also not running out of IP addresses or using less IP addresses. NAT does this by managing an address translation table on a router that connects to the public Internet and the private network so that the addresses of devices on the private network are hidden from the rest of the Internet and the ISP doesn't have to assign IP addresses to each individual device on the network, and can instead just assign one address to the NAT router. Private network addresses can be used on multiple private networks so that only the IP address of NAT router needs to remain unique because it is the only one used over the public Internet. This way less IP addresses are used and devices on private networks can still send packets over the public Internet.  

* **I can explain important differences between IPv4 and IPv6.**  
IPv6 was made in response to the relization that 32-bit IPv4 address space was finite and was beginning to be used at a very quick rate. IPv4 uses 32-bit address whereas IPv6 uses 128-bit address so IPv6 will not runout of addresses. IPv6 also has a new type od address, anycast, which allows a packet to be sent to any one group of hosts which is useful if you want to send a get request to the nearest site out of many sites that are all the same in order to access something.  

* **I can explain how IPv6 datagrams can be sent over networks that only support IPv4.**  
IPv6 datagrams are sent over networks that only support IPv4 by implementing *tunneling*. In this context tunneling works as such:  
 * there are two IPv6 nodes connected to each other via IPv4 routers  
 * the sending IPv6 node will send an IPv4 datagram across the IPv4 routers (or through the *tunnel*) to the receiving IPv6 node  
 * the data field of that IPv4 datagram will contain the whole IPv6 datagram and its protocol number field will be 41  
 * the IPv4 routers will route the datagram among themselves sending it to the IPv6 destination node  
 * once the IPv4 datagram gets to the IPv6 node, it knows that the IPv4 datagram contains an IPv6 datagram because the protocol number field will be set to 41  
 * the receiving IPv6 node then extracts the IPv6 datagram from the IPv4 datagram and proceeds normally from there  