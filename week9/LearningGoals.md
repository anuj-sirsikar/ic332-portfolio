#### Anuj Sirsikar  

## Learning Goals  
### I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.  
* **How it works**: finds the shortest path from a node to all other nodes in the network. So even if that node isn't a direct neighbor to another node, it will calculate the path between them by calculating the path to nodes that neighbor both. And every node will know the distances to other nodes which helps calculate the shortest path. Each node has global information across the network.  
* **Advantages**: all nodes are able to see the whole network (complete view)
* **Problems**: oscillations  
* **Solutions**: don't have all routers running the LS algorithm at the same time. 
* **Example**: OSPF 
### I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.    
* **How it works**: Instead every node over the whole network being able to view the same things and able to have a complete picture of the network like in LS algorithms, here nodes only send and receive information to/from their direct neighbors. The process keeps going on until all information has been exchanged between neighbors. The algorithm self-terminating and does not require all nodes to be in close step with each other.  
* **Advantages**: uses less memory and bandwidth/resources in general and is more simple because doesn't need to know all the paths across the whole network before the calculations star  
* **Problems**: it is slower to converge and count to infinity issues  
* **solutions**: poisoned reverse  
* **Examples**: RIP