#### Anuj Sirsikar  

## Introduction  
This is an interactive story lab where I am in a damaged spaceship and need to communicate via sending messages in order to save my ship. The code is in python and I have to fill in the blanks and change it up to solve the given problems.  
## Collaboration  
Asked Alex Traynor for clarification along the way.  
## Process  
Followed instructions for chapters 1 and 2 from [this](https://gitlab.com/jldowns-usna/protocol-pioneer) gitlab.  
## Questions  
* **Chapter 1**  
Winning strategy: 
``` 
self.send_message("Message Received", "W")  
```
This was the only change I made, because the damaged ship was originally sending a mayday message to the north, but then it received a message from the mothership telling all responsive units to respond with "Message Received". Because the message the mothership was to the west of the damaged ship, I changed the mayday message to say "Message Received" and sent it to the west, in the direction of the mothership.  
* **Chapter 2**  
Winning strategy:  
```
self.state["received"][m.interface][0] += 1  
self.state["received"][m.interface][1] += int(m.text)  
if (self.state["received"][m.interface][0] == 3):  
    self.send_message(self.state["received"][m.interface][1], m.interface)  
```
I am still getting used to python, but what I did here was I used the incoming interface which is the direction and in the first index for each direction I stored the counter (for number of messages received from that direction) and in the second index I stored the sum of the messages. And then once a direction's count reached 3, I sent the sum of those messages back in the direction I received them from.  