#### Anuj Sirsikar  

## Review Questions  
* **R5. What is the "count to infinity" problem in distance vector routing?**  
Say you have three routers, A B and C. A is connected to B and B is connected to C. The path lengths are all 1 so the path from A to B is 1 and the path from B to C is 1 and so the path from A to C is 2. If the connection between B and C is broken, and B gets a message from A headed for C before it has sent out any updates, then A will think that its path to C is still 2. B will no longer have a path to C but it will see that A does and because the path between A and B is 1 and A to C is 2, B will think that it has a path of 3 to C and then A will think that it has a path of 4 to C and on and on. 
  
* **R8. True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.**  
False. OSPF is a link-state protocol that uses Dijkstra's least cost path algorithm to find the shortest paths. Therefore, each router will have a complete view of the whole network so a router broadcasts information to all other routers in the network, not just the ones it directly neighbors.  
  
* **R9. What is it meant by an *area* in an OSPF autonomous system? Why was the concept of an area introduced?**  
There is a hierarchy within an autonomous system. The hierarchy can be configured into areas. Each area has its own OSPF link state routing algorithm. Areas have border routers that route packets outside the area and there is a backbone area that routes traffic between the other areas. The main purpose of area is to support a hierarchy within an AS.  

* **P3. Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.**  
[image of answer in week 9 directory]  
  
* **P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following:**  
**c. Compute the shortest path from v to all network nodes.**  
**e. Compute the shortest path from y to all network nodes.**  
**d. Compute the shortest path from w to all network nodes.**  
  
* **P5. Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z.**  
  
* **P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.**  
**a. When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?**  
  
**b. Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.**  
  
**c. How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?**  